﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace патерни
{
    class Program
    {
        delegate bool Changing();
        static private bool ConstantChange()
        {
            Console.Write($"Назва автомобіля: {Name}\nВведіть нову назву автомобіля: ");
            string New = Console.ReadLine();
            if (New != Name)
            {
                Console.WriteLine($"\t\tНова назва: {New}\n");
            }
            Name = New;

            return true;
        }
        static private bool OptionalChange()
        {
            Console.WriteLine($"Назва автомобіля: {Name}\nНатисніть Y, якщо хочете змінити назву, " +
                $"чи N, якщо хочете завершити роботу");

            do
            {
                var key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.Y:
                        Console.Write("Введіть нову назву автомобіля: ");
                        string New = Console.ReadLine();

                        if (New != Name)
                        {
                            Console.WriteLine($"\t\tНова назва: {New}\n");
                        }
                        Name = New;
                        return true;
                    case ConsoleKey.N:
                        Console.WriteLine("\t\tРоботу завершено!");
                        return false;
                    default:
                        break;
                }
            }
            while (true);
        }

        static string Name = "";

        static void Main()
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;

            CarName(OptionalChange);

            Console.ReadLine();
        }

        static void CarName(Changing changing)
        {
            bool test;
            do
            {
                test = changing();
            }
            while (test);
        }
    }
}