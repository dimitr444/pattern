﻿ using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    [ServiceContract]
    public interface ICalculator
    {
        [OperationContract]
        void AddValue(double val);
        [OperationContract]

        double GetResultValue();
        [OperationContract]

        double Addotoin(double a, double b);
        [OperationContract]

        double Multiple(double a, double b);
        [OperationContract]

        void SendMassage(string mass);
        [OperationContract]

        string StringInvert(string str);
        [OperationContract]

        void OpenWord();
        [OperationContract]
        void OpenExcel();
        [OperationContract]
        void OpenCalculate();
        [OperationContract]
        void buttonExplorer();
        [OperationContract]
        void OpenChrome();
        [OperationContract]
        string[] GetProcesses();
        [OperationContract]
        Bitmap GetScreenshot();
        [OperationContract]
        void KillProcess(int id);
    }
}
