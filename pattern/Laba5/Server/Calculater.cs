﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public class Calculater : ICalculator
    {
        protected double CurrentValue;
        public double Addotoin(double a, double b)
        {
            return a + b;
        }

        public void AddValue(double val)
        {
            CurrentValue += val;
        }

        public double GetResultValue()
        {
            return CurrentValue;
        }

        public double Multiple(double a, double b)
        {
            return a * b;
        }

        public void SendMassage(string mass)
        {

            Label label = (Application.OpenForms["Form1"] as Form1).labelMassage;
            if (label.InvokeRequired)
            {
                label.Invoke(new Action(() =>
                {
                    label.Text = mass;
                }));
            }
            else
            {
                label.Text = mass;
            }
        }

        public string StringInvert(string str)
        {
            return new string(str.Reverse().ToArray());
        }
        public void OpenWord()
        {
            Process.Start("winword");
        }
        public void OpenChrome()
        {
            Process.Start("chrome");
        }
        public void OpenExcel()
        {
            Process.Start("excel");
        }
        public void OpenCalculate()
        {
            Process.Start("calc");
        }
        public void buttonExplorer()
        {
            Process.Start("Explorer.exe");
        }
        public string[] GetProcesses()
        {
            Process[] processes = Process.GetProcesses();
            string[] processesStr = new string[processes.Length];
            try
            {
                for (int i = 0; i < processes.Length; i++)
                {
                    processesStr[i] = $"{processes[i].Id}, {processes[i].ProcessName}";
                }
                return processesStr;
            }
            catch
            {
                return null;
            }
        }
        public Bitmap GetScreenshot()
        {
            Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                Screen.PrimaryScreen.Bounds.Height,
                PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size);

            return bitmap;
        }
        public void KillProcess(int id)
        {
            Process process = Process.GetProcessById(id);
            process.Kill();
            process.WaitForExit();
        }
    }
}
    

