﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Client.SRcacl {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SRcacl.ICalculator")]
    public interface ICalculator {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/AddValue", ReplyAction="http://tempuri.org/ICalculator/AddValueResponse")]
        void AddValue(double val);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/AddValue", ReplyAction="http://tempuri.org/ICalculator/AddValueResponse")]
        System.Threading.Tasks.Task AddValueAsync(double val);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/GetResultValue", ReplyAction="http://tempuri.org/ICalculator/GetResultValueResponse")]
        double GetResultValue();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/GetResultValue", ReplyAction="http://tempuri.org/ICalculator/GetResultValueResponse")]
        System.Threading.Tasks.Task<double> GetResultValueAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/Addotoin", ReplyAction="http://tempuri.org/ICalculator/AddotoinResponse")]
        double Addotoin(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/Addotoin", ReplyAction="http://tempuri.org/ICalculator/AddotoinResponse")]
        System.Threading.Tasks.Task<double> AddotoinAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/Multiple", ReplyAction="http://tempuri.org/ICalculator/MultipleResponse")]
        double Multiple(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/Multiple", ReplyAction="http://tempuri.org/ICalculator/MultipleResponse")]
        System.Threading.Tasks.Task<double> MultipleAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/SendMassage", ReplyAction="http://tempuri.org/ICalculator/SendMassageResponse")]
        void SendMassage(string mass);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/SendMassage", ReplyAction="http://tempuri.org/ICalculator/SendMassageResponse")]
        System.Threading.Tasks.Task SendMassageAsync(string mass);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/StringInvert", ReplyAction="http://tempuri.org/ICalculator/StringInvertResponse")]
        string StringInvert(string str);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/StringInvert", ReplyAction="http://tempuri.org/ICalculator/StringInvertResponse")]
        System.Threading.Tasks.Task<string> StringInvertAsync(string str);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenWord", ReplyAction="http://tempuri.org/ICalculator/OpenWordResponse")]
        void OpenWord();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenWord", ReplyAction="http://tempuri.org/ICalculator/OpenWordResponse")]
        System.Threading.Tasks.Task OpenWordAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenExcel", ReplyAction="http://tempuri.org/ICalculator/OpenExcelResponse")]
        void OpenExcel();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenExcel", ReplyAction="http://tempuri.org/ICalculator/OpenExcelResponse")]
        System.Threading.Tasks.Task OpenExcelAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenCalculate", ReplyAction="http://tempuri.org/ICalculator/OpenCalculateResponse")]
        void OpenCalculate();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenCalculate", ReplyAction="http://tempuri.org/ICalculator/OpenCalculateResponse")]
        System.Threading.Tasks.Task OpenCalculateAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/buttonExplorer", ReplyAction="http://tempuri.org/ICalculator/buttonExplorerResponse")]
        void buttonExplorer();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/buttonExplorer", ReplyAction="http://tempuri.org/ICalculator/buttonExplorerResponse")]
        System.Threading.Tasks.Task buttonExplorerAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenChrome", ReplyAction="http://tempuri.org/ICalculator/OpenChromeResponse")]
        void OpenChrome();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/OpenChrome", ReplyAction="http://tempuri.org/ICalculator/OpenChromeResponse")]
        System.Threading.Tasks.Task OpenChromeAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/GetProcesses", ReplyAction="http://tempuri.org/ICalculator/GetProcessesResponse")]
        string[] GetProcesses();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/GetProcesses", ReplyAction="http://tempuri.org/ICalculator/GetProcessesResponse")]
        System.Threading.Tasks.Task<string[]> GetProcessesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/GetScreenshot", ReplyAction="http://tempuri.org/ICalculator/GetScreenshotResponse")]
        System.Drawing.Bitmap GetScreenshot();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/GetScreenshot", ReplyAction="http://tempuri.org/ICalculator/GetScreenshotResponse")]
        System.Threading.Tasks.Task<System.Drawing.Bitmap> GetScreenshotAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/KillProcess", ReplyAction="http://tempuri.org/ICalculator/KillProcessResponse")]
        void KillProcess(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculator/KillProcess", ReplyAction="http://tempuri.org/ICalculator/KillProcessResponse")]
        System.Threading.Tasks.Task KillProcessAsync(int id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICalculatorChannel : Client.SRcacl.ICalculator, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CalculatorClient : System.ServiceModel.ClientBase<Client.SRcacl.ICalculator>, Client.SRcacl.ICalculator {
        
        public CalculatorClient() {
        }
        
        public CalculatorClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CalculatorClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalculatorClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalculatorClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void AddValue(double val) {
            base.Channel.AddValue(val);
        }
        
        public System.Threading.Tasks.Task AddValueAsync(double val) {
            return base.Channel.AddValueAsync(val);
        }
        
        public double GetResultValue() {
            return base.Channel.GetResultValue();
        }
        
        public System.Threading.Tasks.Task<double> GetResultValueAsync() {
            return base.Channel.GetResultValueAsync();
        }
        
        public double Addotoin(double a, double b) {
            return base.Channel.Addotoin(a, b);
        }
        
        public System.Threading.Tasks.Task<double> AddotoinAsync(double a, double b) {
            return base.Channel.AddotoinAsync(a, b);
        }
        
        public double Multiple(double a, double b) {
            return base.Channel.Multiple(a, b);
        }
        
        public System.Threading.Tasks.Task<double> MultipleAsync(double a, double b) {
            return base.Channel.MultipleAsync(a, b);
        }
        
        public void SendMassage(string mass) {
            base.Channel.SendMassage(mass);
        }
        
        public System.Threading.Tasks.Task SendMassageAsync(string mass) {
            return base.Channel.SendMassageAsync(mass);
        }
        
        public string StringInvert(string str) {
            return base.Channel.StringInvert(str);
        }
        
        public System.Threading.Tasks.Task<string> StringInvertAsync(string str) {
            return base.Channel.StringInvertAsync(str);
        }
        
        public void OpenWord() {
            base.Channel.OpenWord();
        }
        
        public System.Threading.Tasks.Task OpenWordAsync() {
            return base.Channel.OpenWordAsync();
        }
        
        public void OpenExcel() {
            base.Channel.OpenExcel();
        }
        
        public System.Threading.Tasks.Task OpenExcelAsync() {
            return base.Channel.OpenExcelAsync();
        }
        
        public void OpenCalculate() {
            base.Channel.OpenCalculate();
        }
        
        public System.Threading.Tasks.Task OpenCalculateAsync() {
            return base.Channel.OpenCalculateAsync();
        }
        
        public void buttonExplorer() {
            base.Channel.buttonExplorer();
        }
        
        public System.Threading.Tasks.Task buttonExplorerAsync() {
            return base.Channel.buttonExplorerAsync();
        }
        
        public void OpenChrome() {
            base.Channel.OpenChrome();
        }
        
        public System.Threading.Tasks.Task OpenChromeAsync() {
            return base.Channel.OpenChromeAsync();
        }
        
        public string[] GetProcesses() {
            return base.Channel.GetProcesses();
        }
        
        public System.Threading.Tasks.Task<string[]> GetProcessesAsync() {
            return base.Channel.GetProcessesAsync();
        }
        
        public System.Drawing.Bitmap GetScreenshot() {
            return base.Channel.GetScreenshot();
        }
        
        public System.Threading.Tasks.Task<System.Drawing.Bitmap> GetScreenshotAsync() {
            return base.Channel.GetScreenshotAsync();
        }
        
        public void KillProcess(int id) {
            base.Channel.KillProcess(id);
        }
        
        public System.Threading.Tasks.Task KillProcessAsync(int id) {
            return base.Channel.KillProcessAsync(id);
        }
    }
}
