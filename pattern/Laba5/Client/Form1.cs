﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.SRcacl;
using System.ServiceModel.Discovery;
using System.ServiceModel.Diagnostics;
using System.ServiceModel;

namespace Client
{
    public partial class Form1 : Form
    {
        CalculatorClient client;
        double a;
        double b;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            a = Convert.ToInt32(NumA.Text);
            b = Convert.ToInt32(Numb.Text);

            double res = client.Addotoin(a, b);
            labelRESult.Text = res.ToString(); 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0 && listBox1.SelectedItem != null)
                return;

            string url = listBox1.SelectedItem.ToString();
            NetTcpBinding netTcpBinding = new NetTcpBinding();
            netTcpBinding.Security.Mode = SecurityMode.None;
            try { 
            client = new CalculatorClient(
                netTcpBinding,
                new EndpointAddress(url)

                );
                buttonADD.Enabled = true;
                buttonNULL.Enabled = true;
                buttonSEND.Enabled = true;
                dataGridProcesses.Visible = true;
                buttonConnect.Enabled = false;
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message, "Помилка");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonNULL_Click(object sender, EventArgs e)
        {
            a = Convert.ToInt32(NumA.Text);
            b = Convert.ToInt32(Numb.Text);

            double res = client.Multiple(a, b);
            labelRESult.Text = res.ToString();

        }

        private void buttonSEND_Click(object sender, EventArgs e)
        {
            client.SendMassage(textBox1.Text);
        }

        private void buttonDiscovery_Click(object sender, EventArgs e)
        {
            UdpDiscoveryEndpoint udpDiscoveryEndpoint = new UdpDiscoveryEndpoint();
            DiscoveryClient discoveryClient = new DiscoveryClient(udpDiscoveryEndpoint);
            FindCriteria findCriteria = new FindCriteria(typeof(ICalculator));
            findCriteria.Duration = new TimeSpan(0, 0, 2);
            FindResponse response = discoveryClient.Find(findCriteria);
            listBox1.Items.Clear();
            for (int i = 0; i < response.Endpoints.Count; i++)
            {
                listBox1.Items.Add(response.Endpoints[i].Address.ToString());
            }
        }
         
        private void buttonValue_Click(object sender, EventArgs e)
        {
            client.AddValue(1);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            labelRESult.Text = client.GetResultValue().ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void NumA_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonGetProcs_Click(object sender, EventArgs e)
        {
            string[] processes = client.GetProcesses();

            dataGridProcesses.RowCount = processes.Length;
            for (int i = 0; i < processes.Length; i++)
            {
                char[] separators = new char[] { ',', ' ' };
                string id = processes[i].Split(separators)[0];
                string processName = processes[i].Split(separators)[2];

                dataGridProcesses.Rows[i].Cells[0].Value = id;
                dataGridProcesses.Rows[i].Cells[1].Value = processName;

            }
        }

        private void backgroundWorkerCriteria_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void dataGridProcesses_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void buttonKill_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = null;
            for (int i = 0; i < dataGridProcesses.RowCount; i++)
            {
                bool doBreak = false;
                for (int j = 0; j < dataGridProcesses.ColumnCount; j++)
                {
                    if (dataGridProcesses.Rows[i].Cells[j].Selected)
                    {
                        row = dataGridProcesses.Rows[i];

                        doBreak = true;
                        break;
                    }
                }
                if (doBreak)
                {
                    break;
                }
            }
            if (row == null)
            {
                return;
            }
            client.KillProcess(int.Parse(row.Cells[0].Value.ToString()));
            buttonGetProcs.PerformClick();
        }

        private void buttonWord_Click(object sender, EventArgs e)
        {
            client.OpenWord();
        }

        private void buttonExcel_Click(object sender, EventArgs e)
        {
            client.OpenExcel();

        }

        private void buttonCalculator_Click(object sender, EventArgs e)
        {
            client.OpenCalculate();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            client.buttonExplorer();
        }

        private void dataGridProcesses_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonScreenshot_Click(object sender, EventArgs e)
        {
            Screenshot screenshot = new Screenshot(client.GetScreenshot());
            screenshot.Show();

        }

        private void pictureBoxScreenshot_Click(object sender, EventArgs e)
        {

        }
    }
}
 