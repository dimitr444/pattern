﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonADD = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonNULL = new System.Windows.Forms.Button();
            this.buttonSEND = new System.Windows.Forms.Button();
            this.labelRESult = new System.Windows.Forms.Label();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonDiscovery = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.NumA = new System.Windows.Forms.TextBox();
            this.Numb = new System.Windows.Forms.TextBox();
            this.buttonKill = new System.Windows.Forms.Button();
            this.buttonExcel = new System.Windows.Forms.Button();
            this.buttonCalculator = new System.Windows.Forms.Button();
            this.buttonGetProcs = new System.Windows.Forms.Button();
            this.buttonWord = new System.Windows.Forms.Button();
            this.dataGridProcesses = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonScreenshot = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProcesses)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonADD
            // 
            this.buttonADD.Enabled = false;
            this.buttonADD.Location = new System.Drawing.Point(12, 20);
            this.buttonADD.Name = "buttonADD";
            this.buttonADD.Size = new System.Drawing.Size(103, 43);
            this.buttonADD.TabIndex = 0;
            this.buttonADD.Text = "Add";
            this.buttonADD.UseVisualStyleBackColor = true;
            this.buttonADD.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(230, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 50);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сlient";
            // 
            // buttonNULL
            // 
            this.buttonNULL.Enabled = false;
            this.buttonNULL.Location = new System.Drawing.Point(121, 18);
            this.buttonNULL.Name = "buttonNULL";
            this.buttonNULL.Size = new System.Drawing.Size(103, 43);
            this.buttonNULL.TabIndex = 2;
            this.buttonNULL.Text = "Mult";
            this.buttonNULL.UseVisualStyleBackColor = true;
            this.buttonNULL.Click += new System.EventHandler(this.buttonNULL_Click);
            // 
            // buttonSEND
            // 
            this.buttonSEND.Enabled = false;
            this.buttonSEND.Location = new System.Drawing.Point(12, 97);
            this.buttonSEND.Name = "buttonSEND";
            this.buttonSEND.Size = new System.Drawing.Size(103, 43);
            this.buttonSEND.TabIndex = 3;
            this.buttonSEND.Text = "Send";
            this.buttonSEND.UseVisualStyleBackColor = true;
            this.buttonSEND.Click += new System.EventHandler(this.buttonSEND_Click);
            // 
            // labelRESult
            // 
            this.labelRESult.AutoSize = true;
            this.labelRESult.Location = new System.Drawing.Point(121, 97);
            this.labelRESult.Name = "labelRESult";
            this.labelRESult.Size = new System.Drawing.Size(76, 17);
            this.labelRESult.TabIndex = 4;
            this.labelRESult.Text = "Результат";
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(239, 112);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(98, 43);
            this.buttonConnect.TabIndex = 5;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 146);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(212, 101);
            this.textBox1.TabIndex = 6;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttonDiscovery
            // 
            this.buttonDiscovery.Location = new System.Drawing.Point(239, 66);
            this.buttonDiscovery.Name = "buttonDiscovery";
            this.buttonDiscovery.Size = new System.Drawing.Size(98, 40);
            this.buttonDiscovery.TabIndex = 7;
            this.buttonDiscovery.Text = "Discovery";
            this.buttonDiscovery.UseVisualStyleBackColor = true;
            this.buttonDiscovery.Click += new System.EventHandler(this.buttonDiscovery_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(343, 71);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(243, 84);
            this.listBox1.TabIndex = 8;
            // 
            // NumA
            // 
            this.NumA.Location = new System.Drawing.Point(12, 69);
            this.NumA.Name = "NumA";
            this.NumA.Size = new System.Drawing.Size(103, 22);
            this.NumA.TabIndex = 9;
            this.NumA.TextChanged += new System.EventHandler(this.NumA_TextChanged);
            // 
            // Numb
            // 
            this.Numb.Location = new System.Drawing.Point(121, 69);
            this.Numb.Name = "Numb";
            this.Numb.Size = new System.Drawing.Size(103, 22);
            this.Numb.TabIndex = 10;
            // 
            // buttonKill
            // 
            this.buttonKill.Location = new System.Drawing.Point(12, 608);
            this.buttonKill.Name = "buttonKill";
            this.buttonKill.Size = new System.Drawing.Size(163, 34);
            this.buttonKill.TabIndex = 16;
            this.buttonKill.Text = "Знищити процес";
            this.buttonKill.UseVisualStyleBackColor = true;
            this.buttonKill.Click += new System.EventHandler(this.buttonKill_Click);
            // 
            // buttonExcel
            // 
            this.buttonExcel.Location = new System.Drawing.Point(181, 648);
            this.buttonExcel.Name = "buttonExcel";
            this.buttonExcel.Size = new System.Drawing.Size(138, 34);
            this.buttonExcel.TabIndex = 15;
            this.buttonExcel.Text = "Excel";
            this.buttonExcel.UseVisualStyleBackColor = true;
            this.buttonExcel.Click += new System.EventHandler(this.buttonExcel_Click);
            // 
            // buttonCalculator
            // 
            this.buttonCalculator.Location = new System.Drawing.Point(325, 568);
            this.buttonCalculator.Name = "buttonCalculator";
            this.buttonCalculator.Size = new System.Drawing.Size(138, 34);
            this.buttonCalculator.TabIndex = 14;
            this.buttonCalculator.Text = "Калькулятор";
            this.buttonCalculator.UseVisualStyleBackColor = true;
            this.buttonCalculator.Click += new System.EventHandler(this.buttonCalculator_Click);
            // 
            // buttonGetProcs
            // 
            this.buttonGetProcs.Location = new System.Drawing.Point(12, 568);
            this.buttonGetProcs.Name = "buttonGetProcs";
            this.buttonGetProcs.Size = new System.Drawing.Size(163, 34);
            this.buttonGetProcs.TabIndex = 13;
            this.buttonGetProcs.Text = "Знайти процеси";
            this.buttonGetProcs.UseVisualStyleBackColor = true;
            this.buttonGetProcs.Click += new System.EventHandler(this.buttonGetProcs_Click);
            // 
            // buttonWord
            // 
            this.buttonWord.Location = new System.Drawing.Point(181, 568);
            this.buttonWord.Name = "buttonWord";
            this.buttonWord.Size = new System.Drawing.Size(138, 34);
            this.buttonWord.TabIndex = 12;
            this.buttonWord.Text = "Word";
            this.buttonWord.UseVisualStyleBackColor = true;
            this.buttonWord.Click += new System.EventHandler(this.buttonWord_Click);
            // 
            // dataGridProcesses
            // 
            this.dataGridProcesses.AllowUserToAddRows = false;
            this.dataGridProcesses.AllowUserToDeleteRows = false;
            this.dataGridProcesses.AllowUserToResizeColumns = false;
            this.dataGridProcesses.AllowUserToResizeRows = false;
            this.dataGridProcesses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridProcesses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProcesses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridProcesses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProcesses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.ProcessName});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridProcesses.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridProcesses.Location = new System.Drawing.Point(12, 263);
            this.dataGridProcesses.MultiSelect = false;
            this.dataGridProcesses.Name = "dataGridProcesses";
            this.dataGridProcesses.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProcesses.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridProcesses.RowHeadersVisible = false;
            this.dataGridProcesses.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProcesses.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridProcesses.Size = new System.Drawing.Size(574, 299);
            this.dataGridProcesses.TabIndex = 20;
            this.dataGridProcesses.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridProcesses_CellContentClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 64;
            // 
            // ProcessName
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ProcessName.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProcessName.HeaderText = "Назва";
            this.ProcessName.MinimumWidth = 6;
            this.ProcessName.Name = "ProcessName";
            this.ProcessName.ReadOnly = true;
            this.ProcessName.Width = 108;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(181, 608);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 34);
            this.button1.TabIndex = 21;
            this.button1.Text = "Explorer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // buttonScreenshot
            // 
            this.buttonScreenshot.Location = new System.Drawing.Point(12, 648);
            this.buttonScreenshot.Name = "buttonScreenshot";
            this.buttonScreenshot.Size = new System.Drawing.Size(163, 34);
            this.buttonScreenshot.TabIndex = 22;
            this.buttonScreenshot.Text = "Зробити скріншот";
            this.buttonScreenshot.UseVisualStyleBackColor = true;
            this.buttonScreenshot.Click += new System.EventHandler(this.buttonScreenshot_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 705);
            this.Controls.Add(this.buttonScreenshot);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridProcesses);
            this.Controls.Add(this.buttonKill);
            this.Controls.Add(this.buttonExcel);
            this.Controls.Add(this.buttonCalculator);
            this.Controls.Add(this.buttonGetProcs);
            this.Controls.Add(this.buttonWord);
            this.Controls.Add(this.Numb);
            this.Controls.Add(this.NumA);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonDiscovery);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.labelRESult);
            this.Controls.Add(this.buttonSEND);
            this.Controls.Add(this.buttonNULL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonADD);
            this.Name = "Form1";
            this.Text = "Klient";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProcesses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonADD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonNULL;
        private System.Windows.Forms.Button buttonSEND;
        private System.Windows.Forms.Label labelRESult;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonDiscovery;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox NumA;
        private System.Windows.Forms.TextBox Numb;
        private System.Windows.Forms.Button buttonKill;
        private System.Windows.Forms.Button buttonExcel;
        private System.Windows.Forms.Button buttonCalculator;
        private System.Windows.Forms.Button buttonGetProcs;
        private System.Windows.Forms.Button buttonWord;
        private System.Windows.Forms.DataGridView dataGridProcesses;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessName;
        private System.Windows.Forms.Button buttonScreenshot;
    }
}

