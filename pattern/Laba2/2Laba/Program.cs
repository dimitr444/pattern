﻿using Lab_02.programm;
using System;
using System.Collections.Generic;

namespace Lab_02
{
    class Program
    {

        public static string Path = "C:\\Users\\User\\Desktop\\патерни\\2\\Lab-02\\Matrix.txt";
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            var saver = Saver.GetInstance();
            saver.OpenOrCreateFile(Path);
            var matrices = new List<Matrix>();

            for (int i = 0; i < 5; i++)
            {
                var matrix = new Matrix(20, 20);
                matrix.CreateMatrix();
                matrices.Add(matrix);
            }
            foreach (var matrix in matrices)
            {
                saver.PrintMatrix(Path, matrix);
            }

            for (int i = 0; i < matrices.Count; i++)
            {
                Console.WriteLine($"Детермінант {i} матриці: {matrices[i].GetDeterminant()}");
            }

            Matrix sum = matrices[0] * matrices[1];
            saver.PrintMatrix(Path, sum);

            for (int i = 2; i < matrices.Count; i++)
            {
                sum *= matrices[i];
                saver.PrintMatrix(Path, sum);
            }

            Console.WriteLine("Обернена матриця до матриці, з перших 5 матриць: ");
            sum.InversedMatrix().Print();

            for (int i = 0; i < 4; i++)
            {
                matrices[i] = new Matrix(5, 5);
                matrices[i].CreateMatrix();
            }

            sum = matrices[0] * matrices[1];

            for (int i = 2; i < 5; i++)
            {
                sum *= matrices[i];
            }

            saver.PrintMatrix(Path, sum);
            saver.ReadAllMatrices(Path);
            Console.WriteLine($"Кількість матриць в файлі: {saver.ReadAllMatrices(Path).Count}");
            saver.ClearFile(Path);
        }
    }
}
