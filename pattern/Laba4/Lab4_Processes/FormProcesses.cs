﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4_Processes
{
    public partial class FormProcess : Form
    {
        public FormProcess()
        {
            InitializeComponent();
        }

        private void buttonWord_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("winword");
            }
            catch (Exception ex)
            {
                OpenError(ex);
            }
        }

        private void buttonCalculator_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("calc");
            }
            catch (Exception ex)
            {
                OpenError(ex);
            }
        }

        private void buttonExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("excel");
            }
            catch (Exception ex)
            {
                OpenError(ex);
            }
        }

        private void buttonChrome_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("chrome");
            }
            catch (Exception ex)
            {
                OpenError(ex);
            }
        }

        private void buttonExplorer_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("Explorer.exe");
            }
            catch (Exception ex)
            {
                OpenError(ex);
            }
        }

        private void OpenError(Exception ex)
        {
            MessageBox.Show(ex.Message, "Не вдалося відкрити програму", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonGetProcs_Click(object sender, EventArgs e)
        {
            var processes = Process.GetProcesses();
            dataGridProcesses.RowCount = processes.Length;
            for(int i = 0; i < processes.Length; i++)
            {
                dataGridProcesses.Rows[i].Cells[0].Value = processes[i].Id;
                dataGridProcesses.Rows[i].Cells[1].Value = processes[i].ProcessName;

                PerformanceCounter pc = new PerformanceCounter();
                pc.CategoryName = "Process";
                pc.CounterName = "Working Set - Private";
                pc.InstanceName = processes[i].ProcessName;

                decimal memory = (decimal)pc.NextValue();
                string size;
                if (memory < 1024)
                {
                    size = "B";
                }
                else
                {
                    if (memory >= (decimal)Math.Pow(1024, 3))
                    {
                        memory /= (decimal)Math.Pow(1024, 3);
                        size = "GB";
                    }
                    else if (memory >= (decimal)Math.Pow(1024, 2))
                    {
                        memory /= (decimal)Math.Pow(1024, 2);
                        size = "MB";
                    }
                    else
                    {
                        memory /= 1024;
                        size = "KB";
                    }
                }
                memory = Math.Round(memory, 2);

                dataGridProcesses.Rows[i].Cells[2].Value = memory.ToString() + size;
                dataGridProcesses.Rows[i].Cells[3].Value = processes[i].Threads.Count;
                try
                {
                    dataGridProcesses.Rows[i].Cells[4].Value = processes[i].PriorityClass.ToString();
                }
                catch
                {
                    dataGridProcesses.Rows[i].Cells[4].Value = "Нема доступу";
                }
                try
                {
                    dataGridProcesses.Rows[i].Cells[5].Value = processes[i].StartTime.ToString();
                }
                catch
                {
                    dataGridProcesses.Rows[i].Cells[5].Value = "Нема доступу";
                }
            }
        }

        private void buttonKill_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = null;
            for(int i = 0; i < dataGridProcesses.RowCount; i++)
            {
                bool doBreak = false;
                for(int j = 0; j < dataGridProcesses.ColumnCount; j++)
                {
                    if(dataGridProcesses.Rows[i].Cells[j].Selected)
                    {
                        row = dataGridProcesses.Rows[i];

                        doBreak = true;
                        break;
                    }
                }
                if(doBreak)
                {
                    break;
                }
            }
            if(row == null)
            {
                return;
            }
            try
            {
                Process process = Process.GetProcessById(int.Parse(row.Cells[0].Value.ToString()));
                process.Kill();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
