﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;

namespace Lab3_Thread
{
    public partial class FormScrypt : Form
    {
        public FormScrypt()
        {
            InitializeComponent();
        }

        string Key;
        Stopwatch stopwatch = new Stopwatch();
        FileStream openFile;
        FileStream saveFile;
        bool IsCancelled = false;

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            stopwatch.Reset();
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            try
            {
                openFile = openFileDialog.OpenFile() as FileStream;
                Key = textBoxKey.Text;
                if (Key == "")
                {
                    Key = textBoxKey.Text = "key";
                }

                progressBarEncrypting.Visible = buttonStopEncryption.Visible
                    = labelProgress.Visible = true;
            }
            catch
            {
                MessageBox.Show("У ході зчитування файлу виникла помилка, спробуйте ще раз.",
                    "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                string fileName = openFileDialog.FileName;
                int dashIndex = fileName.LastIndexOf('\\');
                int size;
                if (fileName.EndsWith(".crypt"))
                {
                    int dotIndex = fileName.LastIndexOf(".");
                    size = dotIndex - dashIndex - 1;
                    saveFileDialog.FileName = fileName.Substring(dashIndex + 1, size);
                }
                else
                {
                    size = fileName.Length - dashIndex - 1;
                    saveFileDialog.FileName = fileName.Substring(dashIndex + 1, size) + ".crypt";
                }
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    saveFile = saveFileDialog.OpenFile() as FileStream;
                    buttonOpen.Enabled = false;

                    progressBarEncrypting.Value = 0;
                    labelProgress.Text = "00:00:00";
                    backgroundWorker.RunWorkerAsync();

                    stopwatch.Start();
                    timer.Start();
                }
            }
            catch
            {
                MessageBox.Show("У ході збереження файлу виникла помилка, спробуйте ще раз.",
                    "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void backgroundWorker_DoWork(object sender, 
            System.ComponentModel.DoWorkEventArgs e)
        {
            int[] keyBytes = new int[Key.Length];
            for(int i = 0; i < keyBytes.Length; i++)
            {
                keyBytes[i] = Key[i];
            }

            int @char = 0;
            long hasRead = 0;
            
            byte[] bytes = new byte[1024 * 1024];
            while (openFile.Position < openFile.Length)
            {
                int size = openFile.Read(bytes, 0, bytes.Length);

                for (int i = 0; i < size; i++)
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        IsCancelled = true;
                        break;
                    }

                    bytes[i] ^= (byte)keyBytes[@char];

                    @char++;
                    @char %= keyBytes.Length;
                    if ((hasRead + i + 1) % (openFile.Length / 100) == 0)
                    {
                        int percentage = (int)Math.Round((hasRead + i + 1) * 100.0 / openFile.Length);
                        backgroundWorker.ReportProgress(percentage);
                    }
                }

                if(IsCancelled)
                {
                    break;
                }

                saveFile.Write(bytes, 0, size);
                hasRead += size;
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, 
            System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBarEncrypting.Value = e.ProgressPercentage;
            int indexer = labelProgress.Text.LastIndexOf(" -");
            if (indexer != -1)
            {
                labelProgress.Text = labelProgress.Text.Substring(0, indexer);
            }
            labelProgress.Text += $" - {e.ProgressPercentage}%";
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, 
            System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            timer.Stop();
            stopwatch.Stop();
            if (IsCancelled)
            {
                MessageBox.Show("Шифрування зупинено", "Зупинка!");
            }
            else
            {
                long kb = saveFile.Length / 1024;
                long mb = kb / 1024;
                MessageBox.Show($"Шифрування файлу завершено!\n" +
                        $"Час виконання: " +
                        $"{labelProgress.Text.Substring(0, labelProgress.Text.LastIndexOf(" -"))}\n" +
                        $"Розмір: {saveFile.Length} байт" + (
                            kb != 0 ? 
                            " / " + kb.ToString() + "Кб" : 
                            ""
                        ) + (
                            mb != 0 ?
                            " / " + mb.ToString() + "Мб" :
                            ""
                        ) + "\n" +
                        $"Назва: {saveFileDialog.FileName}", "Роботу завершено!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            buttonOpen.Enabled = true;

            openFile.Close();
            saveFile.Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            long time = stopwatch.ElapsedMilliseconds;
            long seconds = time / 1000;
            
            long minutes = seconds / 60;
            seconds %= 60;

            long hours = minutes / 60;
            minutes %= 60;

            labelProgress.Text = $"{hours:d2}:{minutes:d2}:{seconds:d2}" + 
                (
                    labelProgress.Text.LastIndexOf(" -") != -1 ? 
                    labelProgress.Text.Substring(labelProgress.Text.LastIndexOf(" -")) : 
                    ""
                );
        }


        private void buttonStopEncryption_Click(object sender, EventArgs e)
        {
            if(backgroundWorker.IsBusy)
            {
                backgroundWorker.CancelAsync();
            }
        }
    }
}
