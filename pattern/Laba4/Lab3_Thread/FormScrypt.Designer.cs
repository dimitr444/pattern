﻿namespace Lab3_Thread
{
    partial class FormScrypt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.textBoxKey = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.progressBarEncrypting = new System.Windows.Forms.ProgressBar();
            this.labelProgress = new System.Windows.Forms.Label();
            this.buttonStopEncryption = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonOpen
            // 
            this.buttonOpen.Location = new System.Drawing.Point(12, 12);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(161, 36);
            this.buttonOpen.TabIndex = 0;
            this.buttonOpen.Text = "Відкрити файл";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // textBoxKey
            // 
            this.textBoxKey.Location = new System.Drawing.Point(419, 16);
            this.textBoxKey.Name = "textBoxKey";
            this.textBoxKey.Size = new System.Drawing.Size(153, 29);
            this.textBoxKey.TabIndex = 2;
            this.textBoxKey.Text = "ключ";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(202, 19);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(211, 22);
            this.labelPassword.TabIndex = 3;
            this.labelPassword.Text = "Ключ для шифрування:";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // progressBarEncrypting
            // 
            this.progressBarEncrypting.Location = new System.Drawing.Point(12, 54);
            this.progressBarEncrypting.Name = "progressBarEncrypting";
            this.progressBarEncrypting.Size = new System.Drawing.Size(560, 23);
            this.progressBarEncrypting.Step = 1;
            this.progressBarEncrypting.TabIndex = 4;
            this.progressBarEncrypting.Visible = false;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProgress.Location = new System.Drawing.Point(12, 95);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(58, 16);
            this.labelProgress.TabIndex = 5;
            this.labelProgress.Text = "00:00:00";
            this.labelProgress.Visible = false;
            // 
            // buttonStopEncryption
            // 
            this.buttonStopEncryption.Location = new System.Drawing.Point(358, 83);
            this.buttonStopEncryption.Name = "buttonStopEncryption";
            this.buttonStopEncryption.Size = new System.Drawing.Size(214, 36);
            this.buttonStopEncryption.TabIndex = 6;
            this.buttonStopEncryption.Text = "Зупинити шифрування";
            this.buttonStopEncryption.UseVisualStyleBackColor = true;
            this.buttonStopEncryption.Visible = false;
            this.buttonStopEncryption.Click += new System.EventHandler(this.buttonStopEncryption_Click);
            // 
            // FormScrypt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 131);
            this.Controls.Add(this.buttonStopEncryption);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBarEncrypting);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.textBoxKey);
            this.Controls.Add(this.buttonOpen);
            this.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximumSize = new System.Drawing.Size(600, 400);
            this.MinimumSize = new System.Drawing.Size(600, 100);
            this.Name = "FormScrypt";
            this.Text = "Шифрування";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonOpen;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.TextBox textBoxKey;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ProgressBar progressBarEncrypting;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.Button buttonStopEncryption;
    }
}

