﻿using System;
using System.Dynamic;

namespace _1lab
{
    class MainProgram
    {

        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.Write("Матриця на NxN = ");
            int n = int.Parse(Console.ReadLine());
            Matrix A = new Matrix(n, n);
            Matrix B = new Matrix(n, n);

            Console.WriteLine("Матриця А: ");
            A.Create();
            A.Print();

            Console.WriteLine("Матриця В: ");
            B.Create();
            B.Print();

            Console.WriteLine("Додавання А и Б: ");
            Console.WriteLine();
            Matrix C = A + B;
            C.Print();

            Console.WriteLine("Множення А и Б:");
            Console.WriteLine();
            C = A * B;
            C.Print();

            Console.WriteLine("Віднімання А и Б:");
            C = A - B;
            C.Print();

            Console.WriteLine("Визначник А: ");
            double det =  A.CalculateDeterminant();
            Console.WriteLine("Визначник = {0}.", det);

            Console.WriteLine("\nТранспонована матриця A: ");
            A.Swap();
            A.Print();

            Console.WriteLine("\nТранспонована матриця B: ");
            B.Swap();
            B.Print();

            Console.WriteLine("\nОбернена матриця A: ");
            A.InverseThis();
            A.Print();
        }
    }
}